var gulp = require('gulp'),
	style = require('gulp-sass'),
	pug = require('gulp-pug'),
	sourcemaps = require('gulp-sourcemaps'),
	browserSync = require('browser-sync').create();

// pug
gulp.task('pug', function(){
	gulp.src('src/*.pug')
		.pipe(pug({pretty: true}))
		.on('error', console.log)
		.pipe(gulp.dest('build/'))
		.on('end', browserSync.reload);
});

// style
gulp.task('style', function () {
	gulp.src('src/sass/**/*.sass')
		.pipe(sourcemaps.init({loadMaps: true}))
		.pipe(style({outputStyle: 'expanded'}).on('error', style.logError))
		.pipe(sourcemaps.write('/maps'))
		.pipe(gulp.dest('build/css/'))
		.on('end', browserSync.reload);
});

// webserver
gulp.task('webserver', function () {
	browserSync.init({
		server: {
			baseDir: './build/',
			tunnel: true
		}
	});
});

// watch
gulp.task('watch', ['webserver'],function(){
	gulp.watch('src/sass/*.sass',['style']);
	gulp.watch('src/*.pug',['pug']);
});
